import UIKit

class ArticleDetail: UIViewController {
    var category = String()
    var article_detial = String()
    var article_title = String()
    var article_image = String()
    var article_content = String()
    var article_url = String()
    
    
    
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var articleDetail: UILabel!
    
    

  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.downloaded(from:article_image)
        articleTitle.text = article_title
        articleDetail.text = article_content
        
        let font = UIFont(name: "Helvetica", size: 20.0)

        //var height = heightForView(text: articleDetail, font: font, width: 100.0)
    }
    
    @IBAction func sourceButton(_ sender: Any) {
        print("btun")
        print(article_url)
        if let url = URL(string:article_url), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, completionHandler: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
