import Foundation
import UIKit

class NewsListTableViewController: UITableViewController {
    
    var category = String()
    private var articleListVM: ArticleListViewModel!
    /// View which contains the loading text and the spinner
     let loadingView = UIView()

     /// Spinner shown during load the TableView
     let spinner = UIActivityIndicatorView()

     /// Text shown during load the TableView
     let loadingLabel = UILabel()
    
    var article_detial = String()
    var article_title = String()
    var article_image = String()
    var article_content = String()
    var article_url = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLoadingScreen()
        setup()
    }
    

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detailSegue") {
            let vc = segue.destination as! ArticleDetail
            
            vc.article_detial = article_detial
            vc.article_title = article_title
            vc.article_image = article_image
            vc.article_content = article_content
            vc.article_url = article_url
            
            
        }
    }
    
    
    
//    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
//        if (segue.identifier == "categoryDetailSegue")
//        {
//            let vc = segue.destination as! ArticleDetail
//            vc.articleDetail
//        }
//    }
//
    // Set the activity indicator into the main view
    private func setLoadingScreen() {

        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 120
        let height: CGFloat = 30
        let x = (tableView.frame.width / 2) - (width / 2)
        let y = (tableView.frame.height / 2) - (height / 2) - (navigationController?.navigationBar.frame.height)!
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)

        // Sets loading text
        loadingLabel.textColor = .gray
        loadingLabel.textAlignment = .center
        loadingLabel.text = "Loading..."
        loadingLabel.frame = CGRect(x: 0, y: 0, width: 140, height: 30)

        // Sets spinner
        spinner.style = .gray
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()

        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        loadingView.addSubview(loadingLabel)

        tableView.addSubview(loadingView)

    }

    // Remove the activity indicator from the main view
    private func removeLoadingScreen() {

        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
        loadingLabel.isHidden = true

    }
    
    

    
    private func setup() {
        print(Constants.URLs.topHeadlines+"&category=\(category)")
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        Webservice().getArticles(with: Constants.URLs.topHeadlines+"&category=\(category)") {
            articles in
            
            if let articles = articles {
                
                self.articleListVM = ArticleListViewModel(articles: articles)
                DispatchQueue.main.async {
                    self.removeLoadingScreen()
                    self.tableView.reloadData()
                }
            }
            
        }
        
    }
    
    
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        article_detial = articleVM.description
        article_title = articleVM.title
        article_image = articleVM.urlToImage
        article_url = articleVM.url
        article_content = articleVM.content
        
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    
   
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
          return self.articleListVM == nil ? 0 : self.articleListVM.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleListVM.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as? ArticleTableViewCell else {
            fatalError("ArticleTableViewCell not found!")
        }
        
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        cell.configure(for: articleVM)
        return cell
        
    }
    
}
