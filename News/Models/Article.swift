import Foundation

struct ArticleList: Decodable {
    let articles: [Article]
}

struct Article: Decodable {
    let title: String?
    let description: String?
    let author : String?
    let url : String?
    let urlToImage : String?
    let content : String?
    
}


