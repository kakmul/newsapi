import Foundation

class Webservice {
    
    func getArticles(with url: String, completion: @escaping ([Article]?) -> ()) {
        
        
        URLSession.shared.dataTask(with: (URL(string: url) ??  URL(string: Constants.URLs.topHeadlines))!) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                
                let articlesList = try? JSONDecoder().decode(ArticleList.self, from: data)
                completion(articlesList?.articles)
            }
            
        }.resume()
        
    }
    
}
