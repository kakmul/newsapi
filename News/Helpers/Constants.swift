import Foundation

struct Constants {
    
    struct URLs {
        static let topHeadlines = "https://newsapi.org/v2/top-headlines?country=id&apiKey=32aa8416656e40e5b6292587a66c4e02"
        static let searchURL = "https://newsapi.org/v2/everything?apiKey=32aa8416656e40e5b6292587a66c4e02"
    }
    
}
